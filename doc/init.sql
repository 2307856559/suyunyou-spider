﻿USE `spider`;

INSERT INTO `sys_user`(`id`,`username`,`password`,`nickname`,`addtime`,`adduser`,`status`)
 VALUES (1,'admin','e00cf25ad42683b3df678c61f42c6bda','管理员',now(),1,0);

INSERT INTO `sys_config`(`id`,`code`,`name`,`value`,`remark`,`exp1`,`exp2`)
 VALUES (1,'sleep.spider.link','爬取地址下载页面间隔时间(单位:ms)','1000',default,default,default);
INSERT INTO `sys_config`(`id`,`code`,`name`,`value`,`remark`,`exp1`,`exp2`)
 VALUES (2,'sleep.analysis.link','分析链接间隔时间(单位:ms)','300',default,default,default);
INSERT INTO `sys_config`(`id`,`code`,`name`,`value`,`remark`,`exp1`,`exp2`)
 VALUES (3,'sleep.fetcher.page','处理符合规则的页面并下载(单位:ms)','300',default,default,default);
INSERT INTO `sys_config`(`id`,`code`,`name`,`value`,`remark`,`exp1`,`exp2`)
 VALUES (4,'down.link.error.max.num','下载链接失败达到指定次，则移除下载','1',default,default,default);
INSERT INTO `sys_config`(`id`,`code`,`name`,`value`,`remark`,`exp1`,`exp2`)
 VALUES (5,'link.repeat.extract.hour','对链接重复提取间隔(单位:小时)','12',default,default,default);

INSERT INTO `sys_config`(`id`,`code`,`name`,`value`,`remark`,`exp1`,`exp2`)
 VALUES (6,'joblog.save.day','调度记录保存天数','7',default,default,default);
INSERT INTO `sys_config`(`id`,`code`,`name`,`value`,`remark`,`exp1`,`exp2`)
 VALUES (7,'clean.cron','清空调度记录表达式','0 0 23 * * ?',default,default,default);
INSERT INTO `sys_config`(`id`,`code`,`name`,`value`,`remark`,`exp1`,`exp2`)
 VALUES (8,'serv.save.day','已停止的服务保存天数','15',default,default,default);
INSERT INTO `sys_config`(`id`,`code`,`name`,`value`,`remark`,`exp1`,`exp2`)
 VALUES (9,'lock.destroy.time','消耗服务和任务的时间[单位:s]','15',default,default,default);
INSERT INTO `sys_config`(`id`,`code`,`name`,`value`,`remark`,`exp1`,`exp2`)
 VALUES (10,'leader.cron','Leader的时间表达式','0/5 * * * * ?',default,default,default);


--测试sql 爬java资料 
insert  into `spider_site`(`site_id`,`name`,`url`,`domain`,`is_enable`) values (1,'java入门资料','http://www.runoob.com/java/','http://www.runoob.com/java/',1);
insert  into `spider_rule`(`rule_id`,`regex`,`title_select`,`content_select`,`site_id`,`is_enable`) values (1,'http://www.runoob.com/java/[\w|\d|-]+.html','#content>h1','#content',1,1);

/*
--测试sql 爬微信公众号的最新内容
insert  into `spider_site`(`site_id`,`name`,`url`,`domain`,`is_enable`) values (2,'公众号','https://mp.weixin.qq.com/','https://mp.weixin.qq.com/profile?src=3&timestamp=1516936405&ver=1&signature=uPB8oKFEO2MOmtfGg3iI9tPNQr4rvXQravqPzRYhKmWoCu8ytfXavoF5Ty-s74CO5TmvFh8jf*w6jhIkLuBtXQ==',1);
insert  into `spider_rule`(`rule_id`,`regex`,`title_select`,`content_select`,`site_id`,`is_enable`) values (2,'https://mp.weixin.qq.com/profile?src=3&timestamp=1516936405&ver=1&signature=uPB8oKFEO2MOmtfGg3iI9tPNQr4rvXQravqPzRYhKmWoCu8ytfXavoF5Ty-s74CO5TmvFh8jf*w6jhIkLuBtXQ==','#page-content>#img-content>h2','#page-content>#img-content>.rich_media_content',1,1);
*/
