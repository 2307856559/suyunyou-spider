drop index uq_spider_url on page_info;

drop table if exists page_info;

drop index uq_md5_link on spider_link;

drop table if exists spider_link;

drop index uq_regex_site_id on spider_rule;

drop table if exists spider_rule;

drop index uq_url on spider_site;

drop table if exists spider_site;

drop table if exists sys_config;

drop index unique_username on sys_user;

drop table if exists sys_user;

/*==============================================================*/
/* Table: page_info                                             */
/*==============================================================*/
create table page_info
(
   page_id              int not null auto_increment comment '编号',
   title                varchar(200) not null comment '标题',
   content              longtext not null comment '正文',
   html                 longtext not null comment '完整html',
   spider_time          datetime not null comment '爬取时间',
   author               varchar(30) comment '作者',
   spider_url           varchar(255) not null comment '爬取地址',
   site_id              int not null comment '来源域名编号',
   primary key (page_id)
);

alter table page_info comment '爬取的页面信息表';

/*==============================================================*/
/* Index: uq_spider_url                                         */
/*==============================================================*/
create unique index uq_spider_url on page_info
(
   spider_url
);

/*==============================================================*/
/* Table: spider_link                                           */
/*==============================================================*/
create table spider_link
(
   link_id              int not null auto_increment comment '编号',
   link                 varchar(255) not null comment '链接',
   md5_link             varchar(150) not null comment 'md5(link)',
   domain               varchar(255) not null comment '来源域名',
   content              longtext comment '内容',
   create_time          datetime not null comment '创建时间',
   is_fetcher_content   int not null comment '是否提取内容',
   fetcher_content_time datetime comment '提取内容时间',
   is_fetcher_link      int not null comment '是否提取链接',
   fetcher_link_time    datetime comment '提取链接时间',
   site_id              int not null comment '来源网址编号',
   down_error_num       int not null comment '下载失败次数',
   primary key (link_id)
);

alter table spider_link comment '爬取的链接内容表';

/*==============================================================*/
/* Index: uq_md5_link                                           */
/*==============================================================*/
create unique index uq_md5_link on spider_link
(
   md5_link
);

/*==============================================================*/
/* Table: spider_rule                                           */
/*==============================================================*/
create table spider_rule
(
   rule_id              int not null auto_increment comment '编号',
   regex                varchar(200) not null comment '页面表达式',
   title_select         varchar(100) not null comment '标题选择器',
   content_select       varchar(100) not null comment '内容选择器',
   site_id              int not null comment '网站编号',
   is_enable            int not null comment '是否启用[0否、1是]',
   primary key (rule_id)
);

alter table spider_rule comment '爬取规则表';

/*==============================================================*/
/* Index: uq_regex_site_id                                      */
/*==============================================================*/
create unique index uq_regex_site_id on spider_rule
(
   regex,
   site_id
);

/*==============================================================*/
/* Table: spider_site                                           */
/*==============================================================*/
create table spider_site
(
   site_id              int not null auto_increment comment '编号',
   name                 varchar(50) not null comment '名称',
   url                  varchar(255) not null comment '爬取地址',
   domain               varchar(255) not null comment '域名',
   is_enable            int not null comment '是否启用[0否、1是]',
   primary key (site_id)
);

alter table spider_site comment '爬取网站表';

/*==============================================================*/
/* Index: uq_url                                                */
/*==============================================================*/
create unique index uq_url on spider_site
(
   url
);

/*==============================================================*/
/* Table: sys_config                                            */
/*==============================================================*/
create table sys_config
(
   id                   int not null auto_increment comment '编号',
   code                 varchar(50) not null comment '编码',
   name                 varchar(50) not null comment '名称',
   value                varchar(100) not null comment '值',
   remark               varchar(100) comment '描叙',
   exp1                 varchar(100) comment '扩展1',
   exp2                 varchar(100) comment '扩展2',
   primary key (id)
);

/*==============================================================*/
/* Table: sys_user                                              */
/*==============================================================*/
create table sys_user
(
   id                   int not null auto_increment comment '编号',
   username             varchar(30) not null comment '用户名',
   password             varchar(80) not null comment '密码',
   nickname             varchar(30) not null comment '昵称',
   addtime              datetime not null comment '添加时间',
   adduser              int not null comment '添加人',
   status               int not null comment '状态【0正常、1冻结】',
   primary key (id)
);

/*==============================================================*/
/* Index: unique_username                                       */
/*==============================================================*/
create unique index unique_username on sys_user
(
   username
);
