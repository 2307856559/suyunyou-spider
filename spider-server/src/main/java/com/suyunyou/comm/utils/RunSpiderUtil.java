package com.suyunyou.comm.utils;

import java.util.List;

import org.apache.log4j.Logger;

import com.suyunyou.manager.pojo.SpiderRule;
import com.suyunyou.manager.pojo.SpiderSite;
import com.suyunyou.manager.service.SpiderRuleService;
import com.suyunyou.manager.service.SpiderSiteService;
import com.suyunyou.spider.Spider;
import com.suyunyou.spider.data.FullData;
import com.suyunyou.spider.data.SiteData;
import com.suyunyou.spider.utils.SpiderUtil;
import com.system.comm.utils.FrameSpringBeanUtil;

/**
 * 初始化和重置
 * @author yuejing
 * @date 2016年6月26日 下午5:27:21
 * @version V1.0.0
 */
public class RunSpiderUtil {

	private static final Logger LOGGER = Logger.getLogger(RunSpiderUtil.class);
	private static Spider spider;

	/**
	 * 初始化
	 */
	public static void init() {
		spider = new Spider();
		new Thread(new Runnable() {
			@Override
			public void run() {

				FullData.reset();
				SpiderUtil.clearPluginsFetcherPage();
				initConfig();
				//spider.addSite("http://www.blogjava.net");
				/*String regex = "http://www.blogjava.net/[\w|\d]+/[\w|\d]+/\d+/\d+/\d+/\d+.html";
		String titleSelect = "title";
		String contentSelect = "body .post";
		spider.addSiteFetcherPage(regex, titleSelect, contentSelect);*/
				//spider.addSite("http://www.linuxidc.com");
				//spider.addSite("http://blog.csdn.net");

				spider.run();
			}
		}).start();
	}

	/**
	 * 初始化配置
	 */
	private static void initConfig() {

		SpiderSiteService siteService = FrameSpringBeanUtil.getBean(SpiderSiteService.class);
		List<SpiderSite> sites = siteService.findEnable();
		SiteData.adds(sites);
		for (SpiderSite site : sites) {
			SpiderUtil.addSite(site.getUrl(), site.getSiteId());
		}
		
		SpiderRuleService ruleService = FrameSpringBeanUtil.getBean(SpiderRuleService.class);
		List<SpiderRule> rules = ruleService.findEnable();
		for (SpiderRule rule : rules) {
			SpiderUtil.addSiteFetcherPage(rule.getRegex(), rule.getTitleSelect(), rule.getContentSelect());
		}
	}

	/**
	 * 重置爬虫
	 */
	public static void reset() {
		//分布式 要修改为同时重置所有服务的
		spider.stop();
		init();
		LOGGER.info("重启爬虫服务");
	}
}