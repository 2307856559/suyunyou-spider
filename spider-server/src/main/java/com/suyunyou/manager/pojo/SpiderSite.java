package com.suyunyou.manager.pojo;

import java.io.Serializable;

import org.apache.ibatis.type.Alias;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.system.comm.model.BaseEntity;

/**
 * 提取网站实体
 * @author yuejing
 * @date 2015年4月5日 下午10:09:28
 * @version V1.0.0
 */
@Alias("spiderSite")
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
public class SpiderSite extends BaseEntity implements Serializable {
	//编号
	private Integer siteId;
	//名称
	private String name;
	//爬取地址
	private String url;
	//标题
	private String domain;
	//是否启用[0否、1是]
	private Integer isEnable;
	
	public SpiderSite() {
		super();
	}
	/*public SpiderSite(String domain) {
		super();
		this.domain = domain;
	}*/
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Integer getSiteId() {
		return siteId;
	}
	public void setSiteId(Integer siteId) {
		this.siteId = siteId;
	}
	public String getDomain() {
		return domain;
	}
	public void setDomain(String domain) {
		this.domain = domain;
	}
	public Integer getIsEnable() {
		return isEnable;
	}
	public void setIsEnable(Integer isEnable) {
		this.isEnable = isEnable;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
}