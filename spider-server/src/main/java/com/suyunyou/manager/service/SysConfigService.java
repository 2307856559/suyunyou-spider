package com.suyunyou.manager.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.suyunyou.manager.dao.SysConfigDao;
import com.suyunyou.manager.enums.Config;
import com.suyunyou.manager.pojo.SysConfig;
import com.system.comm.model.Page;

/**
 * 系统配置的Service
 * @author yuejing
 * @date 2015-03-30 14:07:27
 * @version V1.0.0
 */
@Component
public class SysConfigService {

	@Autowired
	private SysConfigDao sysConfigDao;

	/**
	 * 保存
	 * @param sysConfig
	 */
	public void save(SysConfig sysConfig) {
		sysConfigDao.save(sysConfig);
	}

	/**
	 * 修改
	 * @param sysUser
	 * @throws SchedulerException 
	 */
	public void update(SysConfig sysConfig) {
		sysConfigDao.update(sysConfig);
	}

	/**
	 * 根据id获取对象
	 * @param id
	 * @return
	 */
	public SysConfig get(Integer id) {
		return sysConfigDao.get(id);
	}
	
	/**
	 * 获取值
	 * @param config
	 * @return
	 */
	public String getValue(Config config) {
		return getValue(config, null);
	}

	/**
	 * 根据code获取值
	 * @param id
	 * @param defValue
	 * @return
	 */
	public String getValue(Config config, String defValue) {
		SysConfig sysConfig = sysConfigDao.getByCode(config.getCode());
		if(sysConfig == null) {
			return defValue;
		}
		return sysConfig.getValue();
	}

	/**
	 * 分页获取对象
	 * @param sysUser
	 * @return
	 */
	public Page<SysConfig> pageQuery(SysConfig sysConfig) {
		sysConfig.setDefPageSize();
		int total = sysConfigDao.findSysConfigCount(sysConfig);
		List<SysConfig> rows = null;
		if(total > 0) {
			rows = sysConfigDao.findSysConfig(sysConfig);
		}
		Page<SysConfig> page = new Page<SysConfig>(sysConfig.getPage(), sysConfig.getSize(), total, rows);
		return page;
	}

}