package com.suyunyou.manager.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.suyunyou.manager.dao.SpiderSiteDao;
import com.suyunyou.manager.pojo.SpiderSite;
import com.system.comm.model.KvEntity;

/**
 * 提取网站的Service
 * @author yuejing
 * @date 2016年6月26日 上午8:31:45
 * @version V1.0.0
 */
@Component
public class SpiderSiteService {

	@Autowired
	private SpiderSiteDao spiderSiteDao;
	
	/**
	 * 保存提取网站
	 * @param spiderSite
	 */
	public void saveOrUpdate(SpiderSite spiderSite) {
		if(spiderSite.getSiteId() == null) {
			spiderSiteDao.save(spiderSite);
		} else {
			spiderSiteDao.update(spiderSite);
		}
	}

	/**
	 * 根据编号获取对象
	 * @param siteId
	 * @return
	 */
	public SpiderSite get(Integer siteId) {
		return spiderSiteDao.get(siteId);
	}

	/**
	 * 获取所有提起的网站
	 * @return
	 */
	public List<SpiderSite> findAll() {
		return spiderSiteDao.findAll();
	}

	/**
	 * 删除网站
	 * @param siteId
	 */
	public void delete(Integer siteId) {
		spiderSiteDao.delete(siteId);
	}

	/**
	 * 获取启用的网站
	 * @return
	 */
	public List<SpiderSite> findEnable() {
		return spiderSiteDao.findEnable();
	}

	public List<KvEntity> findKvAll() {
		List<KvEntity> data = new ArrayList<KvEntity>();
		List<SpiderSite> list = findAll();
		for (SpiderSite spiderSite : list) {
			data.add(new KvEntity(spiderSite.getSiteId().toString(), spiderSite.getName()));
		}
		return data;
	}

}