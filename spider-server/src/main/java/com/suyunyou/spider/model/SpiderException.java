package com.suyunyou.spider.model;



/**
 * 异常类
 * @author 岳静
 * @date 2016年6月24日 下午3:36:29 
 * @version V1.0
 */
public class SpiderException extends Exception {
    private String code = "-1";
    private String request;
    private String error;
    private static final long serialVersionUID = -2623309261327598087L;

    public SpiderException(String msg) {
        super(msg);
    }

    public SpiderException(Exception cause) {
        super(cause);
    }
    
    public SpiderException(String code, String msg) {
    	super(msg);
    	this.code = code;
    }

    public SpiderException(String msg, Exception cause) {
        super(msg, cause);
    }

    public SpiderException(String code, String msg, Exception cause) {
        super(msg, cause);
        this.code = code;

    }

    public String getCode() {
        return this.code;
    }

	public String getRequest() {
		return request;
	}

	public String getError() {
		return error;
	}
    
}