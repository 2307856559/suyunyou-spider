package com.suyunyou.spider.model;

import java.io.Serializable;

public class DownLinkError implements Serializable {

	private static final long serialVersionUID = -5919316532915380822L;
	//链接
	private String link;
	//失败次数
	private Integer errorNum;
	
	public String getLink() {
		return link;
	}
	public void setLink(String link) {
		this.link = link;
	}
	public Integer getErrorNum() {
		return errorNum;
	}
	public void setErrorNum(Integer errorNum) {
		this.errorNum = errorNum;
	}
}