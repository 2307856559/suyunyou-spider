package com.suyunyou.spider.data;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import com.suyunyou.manager.pojo.SpiderSite;

/**
 * 爬取网站的数据
 * @author yuejing
 * @date 2018年1月25日 下午2:38:25
 */
public class SiteData {

	private static final Map<Integer, SpiderSite> siteData = new ConcurrentHashMap<Integer, SpiderSite>();
	
	public static void add(SpiderSite spiderSite) {
		siteData.put(spiderSite.getSiteId(), spiderSite);
	}
	
	public static SpiderSite get(Integer siteId) {
		return siteData.get(siteId);
	}

	public static void adds(List<SpiderSite> sites) {
		for (SpiderSite spiderSite : sites) {
			add(spiderSite);
		}
	}
}
