package com.suyunyou.spider.data;

/**
 * 所有数据
 * @author yuejing
 * @date 2016年6月26日 下午7:39:58
 * @version V1.0.0
 */
public class FullData {

	public static void reset() {
		FetcherAnalysisLinkData.clear();
		FetcherPageLinkData.clear();
	}
}
